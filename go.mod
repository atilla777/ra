module ra

go 1.15

require (
	crawshaw.io/sqlite v0.3.2
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	github.com/valyala/fasttemplate v1.2.1 // indirect
)
