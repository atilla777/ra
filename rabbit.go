package main

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"log"
	"net/url"
)

type response struct {
	JobId     string  `json:"job_id"`
	AgentName string  `json:"agent_name"`
	Code      int     `json:"code"`
	Message   string  `json:"message"`
	Result    NmapRun `json:"result"`
}

var channel *amqp.Channel

const exName = "rism_ra"

func rabbitsStart() {
	con := setConnection()
	defer con.Close()
	channel = setChannel(con)
	defer channel.Close()
	consumerStart()
	producerStart()
	fmt.Println("Broker client start")
	loop := make(chan bool)
	<-loop
}

func setConnection() *amqp.Connection {
	con, err := amqp.Dial(getUrl())
	if err != nil {
		log.Fatalf("Broker connection error: %s", err)
	}
	return con
}

func setChannel(con *amqp.Connection) *amqp.Channel {
	ch, err := con.Channel()
	if err != nil {
		log.Fatalf("Broker set channel error: %s", err)
	}
	return ch
}

func agentName() string {
	return viper.GetString("ra.name")
}

func serverName() string {
	return viper.GetString("rism.name")
}

func getUrl() string {
	u := &url.URL{
		Scheme: "amqp",
		Host:   (viper.GetString("ra.broker.host") + ":" + string(viper.GetString("ra.broker.port"))),
		User:   url.UserPassword(viper.GetString("ra.broker.user"), viper.GetString("ra.broker.password")),
	}
	return u.String()
}
