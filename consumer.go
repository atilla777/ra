package main

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"log"
	"strings"
)

func consumerStart() {
	setExchange(channel, exName)
	jobQueue := setJobQueue() // TODO: should it do RISM (producer)
	setJobBinding(jobQueue)
	messages := setConsumer(jobQueue)
	getMessages(messages)
}

func setExchange(ch *amqp.Channel, name string) {
	err := ch.ExchangeDeclare(
		name,    // name
		"topic", // type
		true,    // durable
		false,   // auto-deleted
		false,   // internal
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		log.Fatalf("Broker set exchange error: %s", err)
	}
}

func setJobQueue() *amqp.Queue {
	queue, err := channel.QueueDeclare(
		agentName(), // name (used as RA agent uniq identificator)
		true,        // durable
		false,       // delete when unused
		false,       // exclusive
		false,       // no-wait
		nil,         // arguments
	)
	if err != nil {
		log.Fatalf("Broker set queue error: %s", err)
	}
	return &queue
}

func setJobBinding(q *amqp.Queue) {
	rKey := strings.Join(
		[]string{"rism", "ra", viper.GetString("ra.name"), "jobs"},
		".",
	)
	err := channel.QueueBind(
		q.Name, // queue name
		rKey,   // routing key
		exName, // exchange
		false,
		nil, // arguments
	)
	if err != nil {
		log.Fatalf("Broker set binding error: %s", err)
	}
}

func setConsumer(q *amqp.Queue) <-chan amqp.Delivery {
	mss, err := channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	if err != nil {
		log.Fatalf("Broker set consumer error: %s", err)
	}
	return mss
}

func getMessages(mss <-chan amqp.Delivery) {
	go func() {
		for m := range mss {
			saveJobFromCostumer(&m)
		}
	}()
}

func saveJobFromCostumer(m *amqp.Delivery) {
	var job Job
	json.Unmarshal(m.Body, &job)
	exist, err := cehckIdExistence(job.Id)
	var l raLog
	var r response
	if err != nil {
		mes := fmt.Sprintf("Broker check job %s existence error: %s", job.Id, err)
		l = raLog{Mes: mes, Lev: "err"}
		r = response{JobId: job.Id, Code: 500, Message: mes}
	}
	if exist {
		mes := fmt.Sprintf("Boker job %s dublication error: %s", job.Id, err)
		l = raLog{Mes: mes, Lev: "err"}
		r = response{JobId: job.Id, Code: 500, Message: mes}
	}
	if err := insertRow(job.Id, job.Options); err != nil {
		mes := fmt.Sprintf("Boker save job %s error: %s", job.Id, err)
		l = raLog{Mes: mes, Lev: "err"}
		r = response{JobId: job.Id, Code: 500, Message: mes}
		return
	} else {
		mes := fmt.Sprintf("Scan job %s accepted.", job.Id)
		l = raLog{Mes: mes, Lev: "info"}
		r = response{JobId: job.Id, Code: 200, Message: mes}
	}
	r.AgentName = agentName()
	logChan <- l
	if err := sendFromProducer(r); err != nil {
		mes := fmt.Sprintf("Boker send job %s ack  error: %s", job.Id, err)
		l = raLog{Mes: mes, Lev: "err"}
		logChan <- l
	}
}
