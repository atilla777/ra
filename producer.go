package main

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"log"
	"strings"
)

func producerStart() {
	con := setConnection()
	defer con.Close()
	ch := setChannel(con)
	defer ch.Close()
	resultQueue := setJobResultQueue()
	setResultBinding(resultQueue)
}

func setJobResultQueue() *amqp.Queue {
	queue, err := channel.QueueDeclare(
		serverName(), // name (used as RISM uniq identificator)
		true,         // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	if err != nil {
		log.Fatalf("Broker set queue error: %s", err)
	}
	return &queue
}

func setResultBinding(q *amqp.Queue) {
	rKey := strings.Join(
		[]string{"rism", "rism", viper.GetString("rism.name"), "results"},
		".",
	)
	err := channel.QueueBind(
		q.Name, // queue name
		rKey,   // routing key
		exName, // exchange
		false,
		nil, // arguments
	)
	if err != nil {
		log.Fatalf("Broker set result  binding error: %s", err)
	}
}

func sendResultFromProducer(res NmapRun) error {
	mes := fmt.Sprintf("Scan job %s done.", res.Jid)
	r := response{JobId: res.Jid, Code: 200, Message: mes, Result: res}
	err := sendFromProducer(r)
	return err
}

func sendFromProducer(r response) error {
	j, err := json.Marshal(r)
	if err != nil {
		mes := fmt.Sprintf("Broker job %s marshal error: %s", r.JobId, err)
		r = response{JobId: r.JobId, Code: 500, Message: mes}
		return fmt.Errorf("%s", mes)
	}
	rKey := strings.Join(
		[]string{"rism", "rism", viper.GetString("rism.name"), "results"},
		".",
	)
	err = channel.Publish(
		exName, // exchange
		rKey,   // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/json",
			Body:        []byte(j),
		},
	)
	if err != nil {
		mes := fmt.Sprintf("Broker job %s publicate error: %s", r.JobId, err)
		return fmt.Errorf("%s", mes)
	}
	return err
}
