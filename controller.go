package main

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

const (
	checkExistenceSQL = `SELECT EXISTS (SELECT 1 FROM jobs WHERE id = ?)`
)

// Initialize Echo web framework
func echoStart() {
	e := echo.New()
	e.POST("/scans", createScan())
	e.Use(middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		return key == viper.GetString("ra.secret"), nil
	}))
	if viper.GetString("ra.protocol") == "http" {
		e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%s", viper.GetString("ra.host"), viper.GetString("ra.port"))))
	} else {
		c := viper.GetString("ra.crt")
		k := viper.GetString("ra.key")
		e.Logger.Fatal(e.StartTLS(fmt.Sprintf("%s:%s", viper.GetString("ra.host"), viper.GetString("ra.port")), c, k))
	}
}

// Echo controller: Save recived through API scan job in sqlite database
func createScan() echo.HandlerFunc {
	return func(c echo.Context) error {
		// RISM scan background job id from POST form
		id := c.FormValue("id")
		exist, err := cehckIdExistence(id)
		if err != nil {
			logChan <- raLog{Mes: fmt.Sprintf("Nmap scan controller check existence error: %s", err), Lev: "err"}
			return c.String(http.StatusInternalServerError, "{\"error\": \"failed\"}")
		}
		if exist {
			return c.String(http.StatusNotAcceptable, "{\"error\": \"dublicated\"}")
		}
		// Get options from POST form
		options := c.FormValue("options")
		if err := insertRow(id, options); err != nil {
			logChan <- raLog{Lev: "err", Mes: fmt.Sprintf("Nmap scan controller save job error: %s", err)}
			// TODO add error status
			return c.String(http.StatusInternalServerError, "{\"error\": \"failed\"}")
		}
		logChan <- raLog{Lev: "info", Mes: fmt.Sprintf("Scan job %s accepted.", id)}

		return c.String(http.StatusOK, "{\"message\": \"accepted\"}")
	}
}

// Insert job with ID to sqlite database
func insertRow(id string, options string) error {
	createJobSQL := `INSERT INTO jobs (id, options, status, attempts, created_at)
      VALUES (?, ?, ?, ?, ?)`
	err := execSQL(createJobSQL, nil, id, options, 0, 0, time.Now().String())
	if err != nil {
		return err
	}
	return nil
}
